package magre

type ControllerRequest struct {
	Type   string
	LogRQ  *LogRQ  `json:"log"`
	ModeRQ *ModeRQ `json:"mode"`
}
