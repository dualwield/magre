package main

import (
	"fmt"
	"gitlab.com/dualwield/magre/cmd/web/app"
	"sync"
)

func main() {
	fmt.Println("Magre")
	wg := sync.WaitGroup{}
	wg.Add(1)
	a := app.New(&wg)
	a.Start()
	wg.Wait()
}
