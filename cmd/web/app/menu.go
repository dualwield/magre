package app

import (
	"gitlab.com/dualwield/golib/jsx/C"
	"gitlab.com/dualwield/golib/jsx/J"
)

func (a *App) refreshMenu() {
	div := J.Document().Call("createElement", "logDiv")
	div.Set("id", "magre-menu")

	div.Get(C.Style).Set(C.Width, J.Px(480))
	div.Get(C.Style).Set(C.Height, J.Px(40))
	//logDiv.Get(C.Style).Set(C.Border, "solid 2px yellow")
	div.Get(C.Style).Set(C.Position, C.Absolute)
	div.Get(C.Style).Set("boxSizing", "border-box")
	div.Get(C.Style).Set(C.Left, J.Px(8))
	div.Get(C.Style).Set(C.Right, J.Px(8))
	div.Get(C.Style).Set(C.Top, J.Px(2))
	div.Get(C.Style).Set(C.Bottom, J.Px(458))

	//title := J.Document().Call("createElement", "span")
	//title.Set("id", "blackbook-menu-title")
	//title.Set("innerHTML", "Hoshek")
	//title.Get(C.Style).Set(C.Position, C.Absolute)
	//title.Get(C.Style).Set("boxSizing", "border-box")
	//title.Get(C.Style).Set(C.Left, J.Px(150))
	//title.Get(C.Style).Set(C.Right, J.Px(150))
	//title.Get(C.Style).Set(C.Width, J.Px(184))
	//title.Get(C.Style).Set(C.Height, J.Px(35))
	////title.Get(C.Style).Set(C.Border, "solid 2px gray")
	//title.Get(C.Style).Set("textAlign", "center")
	//title.Get(C.Style).Set("color", "white")
	//title.Get(C.Style).Set("fontSize", J.Px(35))
	//title.Get(C.Style).Set("fontFamily", "Share Tech Mono")
	//logDiv.Call("appendChild", title)

	a.menu = div
	a.core.HTML().Call("appendChild", div)
}
