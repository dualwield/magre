package app

import (
	"gitlab.com/dualwield/golib/jsx/J"
	"gitlab.com/dualwield/magre/cmd/web/app/commander"
	"gitlab.com/dualwield/magre/cmd/web/app/console"
	"gitlab.com/dualwield/magre/cmd/web/app/core"
	"gitlab.com/dualwield/magre/cmd/web/app/global"
	"sync"
	"syscall/js"
)

func New(wg *sync.WaitGroup) App {
	return App{
		wg: wg,
	}
}

type App struct {
	wg        *sync.WaitGroup
	commander *commander.UserPrompt
	console   *console.ContentView
	core      *core.Core
	menu      js.Value
	buffer    *Buffer
	//controller *Controller
}

func (a *App) Start() {

	controller := NewController(a)
	global.State.RefreshLocation()

	J.Log(global.State.Path) //TODO dev
	a.core = core.New()
	a.commander = commander.New(controller.UserCommand)
	a.console = console.New()

	global.State.RegisterResizeFunc(a.core.Resize)
	global.State.RegisterResizeFunc(a.commander.Resize)
	global.State.RegisterResizeFunc(a.console.Resize)

	a.refreshMenu()
	global.State.ManualResize()

	a.organize()
}

func (a *App) organize() {
	a.core.HTML().Call("appendChild", a.commander.HTML())
}
