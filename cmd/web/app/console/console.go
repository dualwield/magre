package console

import (
	"gitlab.com/dualwield/golib/jsx/C"
	"gitlab.com/dualwield/golib/jsx/J"
	"syscall/js"
)

func New() *ContentView {
	c := ContentView{}

	c.render()
	c.resize()

	return &c
}

type ContentView struct {
	html js.Value
}

func (c *ContentView) render() {
	c.html = J.Document().Call("createElement", "div")
	c.html.Set("id", "magre-console")
	c.html.Get(C.Style).Set(C.Position, C.Absolute)
}

func (c *ContentView) resize() {

}

func (c *ContentView) Resize() {
	c.resize()
}
