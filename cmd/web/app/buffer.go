package app

import "syscall/js"

type Buffer struct {
	current BufferItem
}

type BufferItem interface {
	Actions() []string
	Interact(string) error
	View() js.Value
}
