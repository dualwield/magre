package app

import (
	"gitlab.com/dualwield/golib/jsx/C"
	"gitlab.com/dualwield/golib/jsx/J"
	"syscall/js"
)

func (a *App) refreshConsole() {
	div := J.Document().Call("createElement", "div")
	div.Set("id", "magre-console")
	div.Get(C.Style).Set(C.Width, J.Px(480))
	div.Get(C.Style).Set(C.Height, J.Px(400))
	div.Get(C.Style).Set(C.Position, C.Absolute)
	div.Get(C.Style).Set("boxSizing", "border-box")
	div.Get(C.Style).Set(C.Left, J.Px(8))
	div.Get(C.Style).Set(C.Right, J.Px(8))
	div.Get(C.Style).Set(C.Top, J.Px(50))
	div.Get(C.Style).Set(C.Bottom, J.Px(50))

	nothing := J.Document().Call("createElement", "div")
	div.Call("appendChild", nothing)

	logDiv := J.Document().Call("createElement", "div")
	logDiv.Get(C.Style).Set(C.Position, "relative")

	a.console = &Console{
		div:  div,
		view: "",
		log: &LogView{
			div: logDiv,
		},
	}
	a.core.HTML().Call("appendChild", div)
}

type Console struct {
	view   string // log, buffer, prompt
	div    js.Value
	log    *LogView
	buffer *BufferView
	prompt *PromptView
}

func (c *Console) View(view string) {
	switch view {
	case "log":
		c.div.Call("replaceChild", c.log.div, c.div.Get("children").Call("item", 0))
		c.view = view
	}
}

type LogView struct {
	div js.Value
}

type BufferView struct {
	div js.Value
}

type PromptView struct {
	div js.Value
}
