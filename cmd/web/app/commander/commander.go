package commander

import (
	"gitlab.com/dualwield/golib/jsx/C"
	"gitlab.com/dualwield/golib/jsx/J"
	"gitlab.com/dualwield/magre/cmd/web/app/global"
	"syscall/js"
)

func New(callback func(string)) *UserPrompt {
	userPrompt := UserPrompt{
		boundary:           J.Document().Call("createElement", "div"),
		controllerCallback: callback,
		icon:               J.Document().Call("createElement", "div"),
		inputActive:        J.Document().Call("createElement", "input"),
		inputShadow:        J.Document().Call("createElement", "input"),
	}

	userPrompt.boundary.Call("appendChild", userPrompt.icon)
	userPrompt.boundary.Call("appendChild", userPrompt.inputActive)
	userPrompt.boundary.Call("appendChild", userPrompt.inputShadow)
	userPrompt.render()
	userPrompt.resize()

	return &userPrompt
}

type UserPrompt struct {
	boundary           js.Value
	controllerCallback func(string)
	icon               js.Value
	inputActive        js.Value
	inputShadow        js.Value
}

func (u *UserPrompt) HTML() js.Value {
	return u.boundary
}

func (u *UserPrompt) resize() {
	u.boundary.Get(C.Style).Set(C.Width, J.Px(global.State.AppWidth-20))
	u.boundary.Get(C.Style).Set(C.Height, J.Px(35))
	u.boundary.Get(C.Style).Set(C.Left, J.Px(8))
	u.boundary.Get(C.Style).Set(C.Right, J.Px(8))
	u.boundary.Get(C.Style).Set(C.Top, J.Px(global.State.VoidHeight-70)) //TODO logic behind this number?
	u.boundary.Get(C.Style).Set(C.Bottom, J.Px(5))

	u.icon.Get(C.Style).Set(C.Width, J.Px(35))
	u.icon.Get(C.Style).Set(C.Height, J.Px(35))

	u.inputActive.Get(C.Style).Set(C.Width, J.Px(global.State.AppWidth-70))
	u.inputActive.Get(C.Style).Set(C.Height, J.Px(30))
	u.inputActive.Get(C.Style).Set(C.Left, J.Px(40))
	u.inputActive.Get(C.Style).Set("zIndex", 2)
	u.inputActive.Get(C.Style).Set("fontSize", J.Px(25))

	u.inputShadow.Get(C.Style).Set(C.Width, J.Px(global.State.AppWidth-70))
	u.inputShadow.Get(C.Style).Set(C.Height, J.Px(30))
	u.inputShadow.Get(C.Style).Set(C.Left, J.Px(40))
	u.inputShadow.Get(C.Style).Set("fontSize", J.Px(25))
}

func (u *UserPrompt) Resize() {
	u.resize()
}

func (u *UserPrompt) render() {
	u.boundary.Set("id", "magre-command")
	u.boundary.Get(C.Style).Set(C.Position, C.Absolute)
	u.boundary.Get(C.Style).Set("boxSizing", "border-box")
	u.boundary.Get(C.Style).Set("backgroundColor", "rgba(0, 0, 0, 0.7)")

	u.icon.Set("id", "magre-command-icon")
	u.icon.Get(C.Style).Set(C.Position, C.Absolute)
	u.icon.Get(C.Style).Set("backgroundImage", "url(\"/static/virtual-marker.svg\")")

	u.inputActive.Set("id", "magre-command-input")
	u.inputActive.Get(C.Style).Set(C.Position, C.Absolute)
	u.inputActive.Set("placeholder", "type in command here")
	u.inputActive.Get(C.Style).Set("backgroundColor", "transparent")
	u.inputActive.Get(C.Style).Set("color", "white")
	u.inputActive.Get(C.Style).Set("fontFamily", "Share Tech Mono")

	u.inputShadow.Set("id", "magre-command-input-shadow")
	u.inputShadow.Get(C.Style).Set(C.Position, C.Absolute)
	u.inputShadow.Get(C.Style).Set("backgroundColor", "transparent")
	u.inputShadow.Get(C.Style).Set("color", "gray")
	u.inputShadow.Get(C.Style).Set("fontFamily", "Share Tech Mono")
	u.inputShadow.Get(C.Style).Set("zIndex", 1)
	u.inputShadow.Set("disabled", true)

	u.inputActive.Call(C.AddEventListener, "keydown", js.FuncOf(u.keyboardListener))
}

func (u *UserPrompt) keyboardListener(_ js.Value, args []js.Value) interface{} {
	if args[0].Get("key").String() == "Enter" {
		J.Log("pushed enter")
		u.controllerCallback(u.inputActive.Get("value").String())
		u.inputActive.Set("value", "")
	}
	return nil
}
