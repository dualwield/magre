package core

import (
	"gitlab.com/dualwield/golib/jsx/C"
	"gitlab.com/dualwield/golib/jsx/J"
	"gitlab.com/dualwield/magre/cmd/web/app/global"
	"syscall/js"
)

func New() *Core {
	c := Core{}
	c.render()
	c.resize()

	return &c
}

type Core struct {
	html js.Value
}

func (c *Core) render() {
	body := J.Document().Get(C.Body)
	body.Get(C.Style).Set("backgroundColor", "black")
	c.html = J.Document().Call(C.GetElementByID, "magre-core")
	c.html.Get(C.Style).Set(C.Position, C.Absolute)
	c.html.Get(C.Style).Set("boxSizing", "border-box")
	c.html.Get(C.Style).Set("borderColor", "white")
	c.html.Get(C.Style).Set("borderStyle", "dotted")
}

func (c *Core) resize() {
	c.html.Get(C.Style).Set(C.Width, J.Px(global.State.AppWidth))
	c.html.Get(C.Style).Set(C.Height, J.Px(global.State.AppHeight))
	leftRight := (global.State.VoidWidth - global.State.AppWidth) / 2
	topBottom := (global.State.VoidHeight - global.State.AppHeight) / 2
	c.html.Get(C.Style).Set(C.Left, J.Px(leftRight))
	c.html.Get(C.Style).Set(C.Right, J.Px(leftRight))
	c.html.Get(C.Style).Set(C.Top, J.Px(topBottom))
	c.html.Get(C.Style).Set(C.Bottom, J.Px(topBottom))
	c.html.Get(C.Style).Set("borderWidth", J.Px(2))
}

func (c *Core) HTML() js.Value {
	return c.html
}

func (c *Core) Resize() {
	c.resize()
}
