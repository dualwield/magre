package app

import (
	"fmt"
	"gitlab.com/dualwield/golib/jsx/J"
	"gitlab.com/dualwield/magre"
	"syscall/js"
)

func NewController(app *App) *Controller {
	ws := js.Global().Get("WebSocket").New("ws://localhost:8077/ws/")
	J.Logjs(ws)
	//ws.Call("send", "hello world")
	return &Controller{
		app: app,
		ws:  ws,
	}

}

type Controller struct {
	app *App
	ws  js.Value
}

func (c *Controller) ServiceCommand(rq magre.ControllerRequest) {
	switch rq.Type {
	case "mode":
		c.app.console.View(rq.ModeRQ.ChangeTo)
	case "log":
		newItem := J.Document().Call("createElement", "div")
		newItem.Set("innerHTML", rq.LogRQ.Item.Text.Value)
		c.app.console.log.div.Call("appendChild", newItem)
	}
}

func (c *Controller) UserCommand(value string) {
	J.Log(fmt.Sprintf("Controller: UserCommand got '%s'", value))
	if value == ":log" {
		c.ServiceCommand(magre.ControllerRequest{
			Type: "mode",
			ModeRQ: &magre.ModeRQ{
				ChangeTo: "log",
			},
		})
	}
	if value == ":log boom" {
		c.ServiceCommand(magre.ControllerRequest{
			Type: "mode",
			ModeRQ: &magre.ModeRQ{
				ChangeTo: "log",
			},
		})

		c.ServiceCommand(magre.ControllerRequest{
			Type: "log",
			LogRQ: &magre.LogRQ{
				Item: magre.LogItem{
					Timestamp: 0,
					Type:      "text",
					Text: magre.Text{
						Value: "boom",
					},
				},
			},
		})
	}

	c.ws.Call("send", value)
}
