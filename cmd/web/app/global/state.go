package global

import (
	"gitlab.com/dualwield/golib/jsx/C"
	"gitlab.com/dualwield/golib/jsx/J"
	"syscall/js"
)

var (
	State *state = nil
)

func newState() {
	s := state{
		VoidWidth:  J.Window().Get(C.InnerWidth).Int(),
		VoidHeight: J.Window().Get(C.InnerHeight).Int(),
	}
	s.RefreshLocation()

	voidResize := func(_ js.Value, _ []js.Value) interface{} {
		s.VoidWidth = J.Window().Get(C.InnerWidth).Int()
		s.VoidHeight = J.Window().Get(C.InnerHeight).Int()
		s.AppWidth = s.VoidWidth - 25
		s.AppHeight = s.VoidHeight - 25
		for _, fn := range s.resizeFNs {
			fn()
		}
		return nil
	}

	J.Window().Call(C.AddEventListener, "resize", js.FuncOf(voidResize))
	s.ManualResize = func() {
		voidResize(js.Null(), nil)
	}

	State = &s
}

type state struct {
	Mode         string
	Path         string
	VoidWidth    int
	VoidHeight   int
	AppWidth     int
	AppHeight    int
	ManualResize func()

	resizeFNs []func()
}

func (s *state) RefreshLocation() {
	location := J.Window().Get("location")
	s.Path = location.Get("pathname").String()
	s.Mode = "todo"
}

func (s *state) RegisterResizeFunc(fn func()) {
	s.resizeFNs = append(s.resizeFNs, fn)
}
