package commander

import "github.com/rs/zerolog/log"

type Service struct {
}

func (s *Service) Command(id string, msg []string) {
	log.Info().Strs("command", msg).Str("user", id).Caller().Send()
}
