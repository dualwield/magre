package data

import (
	"github.com/dgraph-io/badger/v4"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"os"
	"sync"
)

func init() {
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
}

func New(dbLocation string) *Service {
	options := badger.DefaultOptions(dbLocation)
	db, err := badger.Open(options)
	if err != nil {
		log.Error().Err(err).Stack().Send()
		return nil
	}

	service := Service{
		db:  db,
		mut: sync.Mutex{},
	}

	return &service
}

type Service struct {
	db  *badger.DB
	mut sync.Mutex
}

func (s *Service) Close() {
	if err := s.db.Close(); err != nil {
		log.Error().Err(err).Send()
	}
}
