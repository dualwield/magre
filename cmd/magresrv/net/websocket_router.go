package net

import (
	"github.com/rs/zerolog/log"
)

func msgRouter(incoming []byte) error {
	msg := string(incoming)

	log.Info().Str("incoming", msg).Send()
	//switch code {
	//case 1:
	//	//TODO the payload for code 1 should be an email address
	//	//TODO read the email, check against its hash in database, if found skip past the truename request
	//	fmt.Println("VAL1 Email") //TODO log
	//case 2:
	//	//TODO the payload for code 2 should be a username
	//	//TODO check the database to make sure the username is available, if not request mode 21 [username with message]
	//	fmt.Println("VAL2 username") //TODO log
	//case 3:
	//	//TODO the payload for code 3 should be a 7 character validation code sent to email
	//	//TODO if code matches code generated in successful VAL2 or VAL21 rs VAL4 with a signed WTClaim
	//	fmt.Println("VAL3 7charcode") //TODO log
	//}

	return nil //TODO proper messaging should not get to this point
}
