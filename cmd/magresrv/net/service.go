package net

import (
	"github.com/gorilla/mux"
	"github.com/gorilla/websocket"
	"github.com/rs/zerolog/log"
	"gitlab.com/dualwield/magre/cmd/magresrv/commander"
	"gitlab.com/dualwield/magre/cmd/magresrv/data"
	"io/fs"
	"net/http"
	"os"
	"strings"
)

func New(ds *data.Service, staticFS fs.FS) *Service {
	return &Service{
		data:   ds,
		static: http.FS(staticFS),
		wsUpgrader: websocket.Upgrader{
			ReadBufferSize:  1024,
			WriteBufferSize: 1024,
		},
		commander: &commander.Service{},
	}
}

type Service struct {
	data       *data.Service
	static     http.FileSystem
	wsUpgrader websocket.Upgrader
	commander  *commander.Service
}

func (s *Service) Start() error {
	router := mux.NewRouter()
	if wasmFile := os.Getenv("MAGRE_WASM"); wasmFile != "" {
		router.HandleFunc("/static/magre.wasm", func(w http.ResponseWriter, r *http.Request) {
			b, err := os.ReadFile(wasmFile)
			if err != nil {
				log.Error().Err(err).Stack().Send()
				w.WriteHeader(http.StatusInternalServerError)
				return
			}
			w.Header().Add("Content-Type", "application/wasm")
			w.Write(b)
		})
	}
	router.HandleFunc("/ws/", s.handleWebsocket).Methods(http.MethodGet)
	router.HandleFunc("/", s.handleRoot).Methods(http.MethodGet)
	router.PathPrefix("/").Handler(http.FileServer(s.static))

	http.Handle("/", router)
	return http.ListenAndServe(":8077", nil)
}

func (s *Service) handleWebsocket(w http.ResponseWriter, r *http.Request) {

	conn, err := s.wsUpgrader.Upgrade(w, r, nil)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	log.Debug().Msg("client connect")

	//TODO establish user id temp is xyz123
	for {
		x, message, err := conn.ReadMessage()
		if err != nil {
			if x == -1 {
				log.Debug().Msg("client disconnect")
				return
			}
			log.Error().Err(err).Int("type", x).Caller().Send()
			return
		}
		splitMsg := strings.Split(string(message), " ")
		s.commander.Command("xyz123", splitMsg)
		if err := msgRouter(message); err != nil {
			log.Error().Err(err).Caller().Send()
			return
		}
	}

}

func (s *Service) handleRoot(w http.ResponseWriter, r *http.Request) {
	const index = `
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Magre</title>
        <script src="/static/wasm_exec.js"></script>
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Oxygen+Mono&family=Play:wght@400;700&family=Share+Tech+Mono&display=swap" rel="stylesheet">
    </head>
    <body>

        <div id="magre-core"></div>

        <script>
            const go = new Go();
            WebAssembly.instantiateStreaming(fetch("/static/magre.wasm"), go.importObject).then((rs) => {
                go.run(rs.instance)
            });
        </script>
    </body>
</html>
`
	b := []byte(index)
	w.Header().Set("Content-Type", "text/html")
	w.Write(b)
}
