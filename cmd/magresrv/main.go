package main

import (
	"embed"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"gitlab.com/dualwield/magre/cmd/magresrv/data"
	"gitlab.com/dualwield/magre/cmd/magresrv/net"
	"os"
	"os/signal"
	"syscall"
)

//go:embed static/*
var embeddedStatic embed.FS

func main() {
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
	dataService := data.New("magre-data")
	netService := net.New(dataService, embeddedStatic)

	// Handle OS Signals
	signals := make(chan os.Signal, 1)
	signal.Notify(signals, syscall.SIGINT, syscall.SIGTERM)
	go func() {
		<-signals
		log.Info().Msg("closing maxwell")
		dataService.Close()
		os.Exit(0)
	}()

	if err := netService.Start(); err != nil {
		log.Error().Err(err).Send()
	}

}
