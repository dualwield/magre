package magre

type LogRQ struct {
	Item LogItem `json:"item"`
}

type LogItem struct {
	Timestamp int64  `json:"timestamp"`
	Type      string `json:"type"`
	Text      Text   `json:"text"`
}

type Text struct {
	Value string `json:"value"`
}
